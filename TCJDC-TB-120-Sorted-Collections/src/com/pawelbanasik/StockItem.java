package com.pawelbanasik;

// implementuje interface do porownywania - bedzie powownywal StockItem
public class StockItem implements Comparable<StockItem> {
	private final String name;
	private double price;
	private int quantityStock;

	public StockItem(String name, double price) {
		this.name = name;
		this.price = price;
		// konstruktor inicjalizuje na zero quantityStock
		this.quantityStock = 0;
	}

	// przeciaza konstruktor tworzac kolejny
	public StockItem(String name, double price, int quantityStock) {
		this.name = name;
		this.price = price;
		this.quantityStock = quantityStock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		// zeby nie mozna bylo dodawac zero i minusowych/walidacja
		if (price > 0.0) {
			this.price = price;
		}
	}

	public String getName() {
		return name;
	}

	public int getQuantityStock() {
		return quantityStock;
	}

	public void adjustStock(int quantity) {
		// pole plus parametr
		int newQuantity = this.quantityStock + quantity;
		// jezeli parametr wiekszy od zera to pole quantityStock bedzie
		// zwiekszone
		if (newQuantity >= 0) {
			this.quantityStock = newQuantity;
		}
	}

	// bedzie uzywal jako klucz w mapie
	@Override
	public boolean equals(Object obj) {
		System.out.println("Entering StockItem.equals");

		// pierwszy test czy obiekty maja taka sama referencje w pamieci
		if (obj == this) {
			return true;
		}

		// drugi test na nulla i czy to jest ta sama klasa
		if ((obj == null) || (obj.getClass() != this.getClass())) {
			return false;
		}
		
		// trzeci ostatni test porownuje juz Stringi name metoda equals
		// dla Stringow
		String objName = ((StockItem) obj).getName();
		return this.name.equals(objName);
	}

	// bedzie uzywal jako klucz w mapie dlatego musi koniecznie nadpisac
	@Override
	public int hashCode() {
		// ta liczba moze byc jakakolwiek to po prostu dodaje do
		// naszego hashCode liczbe
		// pobiera hashCode name i dodaje sobie liczbe
		return this.name.hashCode() + 31;
	}

	// bedzie porownywal swoje obiekty dlatego musi zdefiniowac jak bedzie to robil
	// poza tym jak zaimplementowal Comparator to musi zaimplementowac
	@Override
	public int compareTo(StockItem o) {
		System.out.println("entering StockItem.compareTo");
		
		// sprawdzamy czy takie same w pamieci
		if (this == o) {
			return 0;
		}
		
		// sprawdzamy najpierw na nulla bo dwa nulle moga sie porownac
		// pozniej metoda compare dla Stringow
		if (o != null) {
			return this.name.compareTo(o.getName());
		}

		throw new NullPointerException();

	}

	
	@Override
	public String toString() {
		return this.name + " : price " + this.price;
	}

}
